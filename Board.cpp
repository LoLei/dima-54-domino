#include "Board.h"


Board::Board()
{
}

Board::~Board()
{
}

void Board::fillVector(std::vector<Domino>& all_tiles)
{
  // There are 28 possible tiles, more with double
  Domino temp_domino(0, 0, 0);
  for (int i = 0; i < 49; i++)
  {
    all_tiles.push_back(temp_domino);
  }

  int x = 0;
  int y = 0;
  for (auto& it : all_tiles)
  {
    it.setX(x);
    it.setY(y);
    y++;
    if (y == 7)
    {
      y = 0;
    }
    if (y == 0)
    {
      x++;
    }
  }
}

void Board::setDoubleStatus(std::vector<Domino>& all_tiles)
{
  // Set double status for tiles which are twice in the set
  for (int i = 0; i < 49; i++)
  {
    for (int j = 1; j < 49; j++)
    {
      if (all_tiles[i].getX() == all_tiles[j].getY() &&
        all_tiles[i].getY() == all_tiles[j].getX())
      {
        if (all_tiles[i].getX() != all_tiles[j].getX())
        {
          all_tiles[i].setDouble();
        }
      }
    }
  }
}

void Board::removeTiles(std::vector<Domino>& all_tiles)
{
  // Remove tiles which are twice in the set
  for (std::vector<Domino>::iterator it = all_tiles.begin();
    it != all_tiles.end();)
  {
    if (it->getX() == DOUBLE)
    {
      it = all_tiles.erase(it);
    }
    else
    {
      it++;
    }
  }
}

void Board::setIds(std::vector<Domino>& all_tiles)
{
  int id = 1;
  for (auto& it : all_tiles)
  {
    it.setId(id);
    id++;
  }
}

void Board::printTiles(const std::vector<Domino>& all_tiles)
{
  for (auto it : all_tiles)
  {
    std::cout << '[' << it.getX() << '-' << it.getY() << ']';
    std::cout << " ID: " << it.getId() << std::endl;
  }
}

int Board::checkCircle(std::vector<Domino>& all_tiles)
{
  // See if circle is possible
  // y of first tile has to be connected to x of next tile
  // TODO brute force check if circle is possible
  int i = 0;
  int j = 1;
  while (i < 27)
  {
    j = 1;
    while (j < 27)
    {
      if (all_tiles[i].getY() == all_tiles[j].getX() ||
        all_tiles[i].getY() == all_tiles[j].getY())
      {
        if (all_tiles[0].getId() == all_tiles[j].getId())
        {
          return 1;
        }
        j++;
        i++;
      }
      //j++;
    }
  }
}
