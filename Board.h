#include "Domino.h"
#include <vector>
#include <iostream>

#pragma once
class Board
{
public:
  Board();
  virtual ~Board();

  void fillVector(std::vector<Domino>& all_tiles);
  void setDoubleStatus(std::vector<Domino>& all_tiles);
  void removeTiles(std::vector<Domino>& all_tiles);
  void setIds(std::vector<Domino>& all_tiles);
  void printTiles(const std::vector<Domino>& all_tiles);
  int checkCircle(std::vector<Domino>& all_tiles);
};