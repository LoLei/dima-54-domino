#include "Domino.h"

Domino::Domino()
{
  x_ = 0;
  y_ = 0;
}

Domino::Domino(int x, int y, int id)
{
  x_ = x;
  y_ = y;
  id_ = id;
}

Domino::~Domino()
{
}

int Domino::getX()
{
  return x_;
}

int Domino::getY()
{
  return y_;
}

int Domino::getId()
{
  return id_;
}

void Domino::setX(int x)
{
  x_ = x;
}

void Domino::setY(int y)
{
  y_ = y;
}

void Domino::setId(int id)
{
  id_ = id;
}

void Domino::setDouble()
{
  x_ = DOUBLE;
  y_ = DOUBLE;
}