#include "Double.h"
#include <vector>

#pragma once

// Each domino tile has 0-6 on both sides.
// x is one side, y is other side
class Domino
{
private:
  int x_;
  int y_;
  int id_;
public:
  Domino();
  Domino(int x, int y, int id);
  virtual ~Domino();

  int getX();
  int getY();
  int getId();

  void setX(int x);
  void setY(int y);
  void setId(int id);
  void setDouble();
};

