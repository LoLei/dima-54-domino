#include <iostream>
#include <vector>
#include "Domino.h"
#include "Board.h"

int main()
{
  std::vector<Domino> all_tiles;
  Board* board = new Board();
  board->fillVector(all_tiles);
  board->setDoubleStatus(all_tiles);
  board->removeTiles(all_tiles);
  board->setIds(all_tiles);
  board->printTiles(all_tiles);
  if (board->checkCircle(all_tiles))
  {
    std::cout << "Possible" << std::endl;
  }
  else
  {
    std::cout << "Not possible" << std::endl;
  }
  return 0;
}